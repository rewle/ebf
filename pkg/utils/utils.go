package utils

// PanicOnErr - helper. Used for panic if err != nil
func PanicOnErr(err error) {
	if err != nil {
		panic(err)
	}
}
