package utils

import (
	"os"

	"go.uber.org/zap"
)

var logger *zap.SugaredLogger

func envLogger() (*zap.Logger, error) {
	if os.Getenv("DEBUG") != "" {
		return zap.NewDevelopment()
	}
	return zap.NewProduction()
}

// GetLogger - simple func to return dev or prod logger, singleton
func GetLogger() (*zap.SugaredLogger, error) {
	if logger != nil {
		return logger, nil
	}

	l, err := envLogger()

	if err != nil {
		return nil, err
	}

	defer l.Sync()
	logger = l.Sugar()

	return logger, nil
}
