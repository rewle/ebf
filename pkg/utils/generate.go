package utils

import (
	"io/ioutil"
	"log"
	"os"
)

func ReadFileByEnv(envName string) []byte {
	cfg := os.Getenv(envName)
	if cfg == "" {
		log.Fatal(envName + " not set!")
	}

	f, err := os.Open(cfg)
	if err != nil {
		log.Fatal("Can't open "+envName+" file", err)
	}
	defer f.Close()

	res, err := ioutil.ReadAll(f)
	if err != nil {
		log.Fatal("Can't read CFG file", err)
	}

	return res
}
