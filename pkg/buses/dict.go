package buses

import (
	"sync"
)

// DictBus - simple dict data bass
type DictBus struct {
	lock sync.Mutex
	data sync.Map
}

func NewDictBus() Bus {
	return &DictBus{
		sync.Mutex{},
		sync.Map{},
	}
}

func (b *DictBus) Read(key string) interface{} {
	res, _ := b.data.Load(key)
	return res
}

func (b *DictBus) Write(key string, data interface{}) {
	b.data.Store(key, data)
}

// GetLock - return lock for
func (b *DictBus) GetLock() *sync.Mutex {
	return &b.lock
}
