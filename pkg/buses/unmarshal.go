package buses

import (
	"encoding/json"
	"errors"
	"sync"
)

//go:generate go run generate.go

type Bus interface {
	Read(key string) interface{}
	Write(key string, data interface{})
	GetLock() *sync.Mutex
}

type BusCreator func() Bus

func (b *BusCreator) UnmarshalJSON(data []byte) error {
	creatorName := ""
	err := json.Unmarshal(data, &creatorName)
	if err != nil {
		return err
	}
	c := creators[creatorName]

	if c == nil {
		return errors.New("databus not found")
	}

	*b = c

	return nil
}
