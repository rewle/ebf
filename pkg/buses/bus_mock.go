package buses

import "sync"

type MockBusRead func(key string) interface{}
type MockBusWrite func(key string, data interface{})
type MockBusLock func() *sync.Mutex

// DictBus - simple dict data bass
type MockBus struct {
	r MockBusRead
	w MockBusWrite
	l MockBusLock
}

func NewMockBus(r MockBusRead, w MockBusWrite, l MockBusLock) Bus {
	return &MockBus{r, w, l}
}

func (b *MockBus) Read(key string) interface{} {
	return b.r(key)
}

func (b *MockBus) Write(key string, data interface{}) {
	b.w(key, data)
}

func (b *MockBus) GetLock() *sync.Mutex {
	return b.l()
}
