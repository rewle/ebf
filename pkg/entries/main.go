package entries

import (
	"encoding/json"
	"errors"

	"bitbucket.org/rewle/ebf/pkg/blocks"
	"bitbucket.org/rewle/ebf/pkg/buses"
)

type Entry interface {
	Serve()
}

type EntryCfg struct {
	Type string `json:"type"`
}

func GetEntry(cfg []byte, bus buses.BusCreator, block blocks.Block) (Entry, error) {
	c := EntryCfg{}
	err := json.Unmarshal(cfg, &c)
	if err != nil {
		return nil, err
	}

	if entry[c.Type] == nil {
		return nil, errors.New("entry not found")
	}

	return entry[c.Type](cfg, bus, block)
}
