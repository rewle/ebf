package entries

import (
	"bytes"
	"encoding/json"
	"net/http"
	"time"

	"bitbucket.org/rewle/ebf/pkg/blocks"
	"bitbucket.org/rewle/ebf/pkg/buses"
	"bitbucket.org/rewle/ebf/pkg/utils"
)

type HTTPCfg struct {
	Listen string `json:"listen"`
}

type HTTP struct {
	Listen     string
	BusCreator buses.BusCreator
	Block      blocks.Block
}

func (e *HTTP) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	public := e.BusCreator()
	private := e.BusCreator()

	// TODO write req to private
	e.Block.Run(&public, &private)

	// TODO private response
	w.Header().Add("Content-Type", "application/json")
	data, err := json.Marshal(public)

	if err != nil {
		http.Error(w, "{}", 502)
	} else {
		http.ServeContent(w, r, "", time.Time{}, bytes.NewReader(data))
	}
}

func (e *HTTP) Serve() {
	l, err := utils.GetLogger()
	if err != nil {
		panic(err)
	}
	l.Fatal(http.ListenAndServe(e.Listen, e))
}

func newHTTP(cfg []byte, bus buses.BusCreator, block blocks.Block) (Entry, error) {
	parsed := HTTPCfg{}
	err := json.Unmarshal(cfg, &parsed)
	if err != nil {
		return nil, err
	}

	return &HTTP{
		parsed.Listen,
		bus,
		block,
	}, nil
}
