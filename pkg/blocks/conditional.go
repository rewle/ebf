package blocks

import (
	"context"

	"bitbucket.org/rewle/ebf/pkg/buses"
	"bitbucket.org/rewle/ebf/pkg/utils"
	"github.com/PaesslerAG/gval"
)

// Toggler - block block provides conditional switch
type Toggler struct {
	parsed  gval.Evaluable
	onTrue  Block
	onFalse Block
}

// Run - standard method for run block
func (f *Toggler) Run(public, private *buses.Bus) {
	bg := map[string]interface{}{
		"public":  *public,
		"private": *private,
	}

	ok, e := f.parsed.EvalBool(context.Background(), bg)

	if e != nil {
		log, _ := utils.GetLogger()
		if log != nil {
			log.Warn("Toggler: ", e)
		}
	}

	if !ok {
		f.onFalse.Run(public, private)
	} else {
		f.onTrue.Run(public, private)
	}
}
