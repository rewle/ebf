package blocks

import (
	"encoding/json"

	"bitbucket.org/rewle/ebf/pkg/buses"
	"bitbucket.org/rewle/ebf/pkg/runners"
	"github.com/PaesslerAG/gval"
)

type Block interface {
	Run(public, private *buses.Bus)
}

type DecodedBlock struct {
	Inner Block
}

type encodedBlock struct {
	Type string `json:"type"`
}

type encodedBlockChildren struct {
	Children []DecodedBlock `json:"children"`
}

type encodedBlockCondition struct {
	Condition string       `json:"condition"`
	OnTrue    DecodedBlock `json:"on_true"`
	OnFalse   DecodedBlock `json:"on_false"`
}

type encodedRunner struct {
	Runner runners.Runner `json:"runner"`
}

func getChildren(data []byte) ([]Block, error) {
	encodedChildren := encodedBlockChildren{}

	err := json.Unmarshal(data, &encodedChildren)
	if err != nil {
		return []Block{}, err
	}

	children := make([]Block, len(encodedChildren.Children))
	for i, c := range encodedChildren.Children {
		children[i] = c.Inner
	}
	return children, nil
}

func (b *DecodedBlock) UnmarshalJSON(data []byte) error {
	e := encodedBlock{}
	err := json.Unmarshal(data, &e)
	if err != nil {
		return err
	}

	switch e.Type {
	case "end":
		*b = DecodedBlock{&End{}}
	case "map":
		children, err := getChildren(data)
		if err != nil {
			return err
		}

		*b = DecodedBlock{&Mapper{children}}
	case "reduce":
		children, err := getChildren(data)
		if err != nil {
			return err
		}

		*b = DecodedBlock{&Reducer{children}}
	case "toggle":
		cond := encodedBlockCondition{}
		err = json.Unmarshal(data, &cond)
		if err != nil {
			return err
		}

		parsed, err := gval.Full().NewEvaluable(cond.Condition)
		if err != nil {
			return err
		}

		*b = DecodedBlock{
			&Toggler{
				parsed,
				cond.OnTrue.Inner,
				cond.OnFalse.Inner,
			},
		}
	case "run":
		runner := encodedRunner{}

		err = json.Unmarshal(data, &runner)
		if err != nil {
			return err
		}

		*b = DecodedBlock{&Runner{runner.Runner}}
	default:
		*b = DecodedBlock{}
	}
	return nil
}
