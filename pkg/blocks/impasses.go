package blocks

import (
	"bitbucket.org/rewle/ebf/pkg/buses"
	"bitbucket.org/rewle/ebf/pkg/runners"
)

type End struct{}

func (b *End) Run(_, _ *buses.Bus) {}

type Runner struct {
	run runners.Runner
}

func (b *Runner) Run(public, private *buses.Bus) {
	b.run(public, private)
}
