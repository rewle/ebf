package blocks

import (
	"sync"

	"bitbucket.org/rewle/ebf/pkg/buses"
)

// Mapper - block provide "map" flow
type Mapper struct {
	children []Block
}

// Run - standard method for run block
func (b *Mapper) Run(public, private *buses.Bus) {
	var wg sync.WaitGroup

	wg.Add(len(b.children))

	for _, child := range b.children {
		go func(child Block, public, private *buses.Bus, wg *sync.WaitGroup) {
			child.Run(public, private)
			wg.Done()
		}(child, public, private, &wg)
	}

	wg.Wait()
}

// Reducer - block block provides "reduce" flow
type Reducer struct {
	children []Block
}

// Run - standard method for run block
func (b *Reducer) Run(public, private *buses.Bus) {
	for _, child := range b.children {
		child.Run(public, private)
	}
}
