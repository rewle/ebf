package blocks

import (
	"sync"
	"testing"

	"bitbucket.org/rewle/ebf/pkg/buses"
)

func TestEnd(t *testing.T) {
	db := buses.NewMockBus(func(_ string) interface{} {
		t.Error("Called Read!")
		return nil
	}, func(_ string, _ interface{}) {
		t.Error("Called Write!")
	}, func() *sync.Mutex {
		t.Error("Called GetLock!")
		return &sync.Mutex{}
	})
	e := End{}
	e.Run(&db, &db)
	t.Log("Ok!")
}

func TestRun(t *testing.T) {
	fRead := func(_ string) interface{} {
		t.Error("Called Read!")
		return nil
	}
	fWrite := func(_ string, _ interface{}) {
		t.Error("Called Write!")
	}
	fLock := func() *sync.Mutex {
		t.Error("Called GetLock!")
		return &sync.Mutex{}
	}
	db1 := buses.NewMockBus(fRead, fWrite, fLock)
	db2 := buses.NewMockBus(fRead, fWrite, fLock)

	called := false

	e := Runner{func(d1, d2 *buses.Bus) {
		if called {
			t.Error("Call twice!")
		}
		called = true
		if d1 != &db1 || d2 != &db2 {
			t.Error("Bad arguments!")
		}
	}}
	e.Run(&db1, &db2)

	if !called {
		t.Error("Run never called!")
	} else {
		t.Log("Ok!")
	}
}
