package blocks

import (
	"sync"
	"testing"

	"bitbucket.org/rewle/ebf/pkg/buses"
	"github.com/PaesslerAG/gval"
)

func getEmptyDB(t *testing.T) *buses.Bus {
	b := buses.NewMockBus(func(_ string) interface{} {
		t.Error("Called Read!")
		return nil
	}, func(_ string, _ interface{}) {
		t.Error("Called Write!")
	}, func() *sync.Mutex {
		t.Error("Called GetLock!")
		return &sync.Mutex{}
	})

	return &b
}

func TestConditionalFalse(t *testing.T) {
	db := getEmptyDB(t)

	called := false

	onTrue := Runner{func(d1, d2 *buses.Bus) {
		called = true
	}}
	onFalse := Runner{func(d1, d2 *buses.Bus) {
		t.Error("Called onFalse!")
	}}

	parsed, err := gval.Full().NewEvaluable("1 < 2")
	if err != nil {
		t.Error("Cant evaluate gval!")
	}
	e := Toggler{
		parsed,
		&onTrue,
		&onFalse,
	}
	e.Run(db, db)
	if !called {
		t.Error("Not called onTrue!")
	}
}

func TestConditionalTrue(t *testing.T) {
	db := getEmptyDB(t)

	called := false

	onFalse := Runner{func(d1, d2 *buses.Bus) {
		called = true
	}}
	onTrue := Runner{func(d1, d2 *buses.Bus) {
		t.Error("Called onTrue!")
	}}

	parsed, err := gval.Full().NewEvaluable("1 > 2")
	if err != nil {
		t.Error("Cant evaluate gval!")
	}
	e := Toggler{
		parsed,
		&onTrue,
		&onFalse,
	}
	e.Run(db, db)
	if !called {
		t.Error("Not called onFalse!")
	}
}

// One of
func TestConditionalContext(t *testing.T) {
	dbPublic := buses.NewDictBus()
	dbPrivate := buses.NewDictBus()

	dbPublic.Write("A", 1)
	dbPrivate.Write("B", 2)

	called := false

	onFalse := Runner{func(d1, d2 *buses.Bus) {
		called = true
	}}

	onTrue := Runner{func(d1, d2 *buses.Bus) {
		t.Error("Called onTrue!")
	}}

	parsed, err := gval.Full().NewEvaluable("private.Read(\"B\") < public.Read(\"A\")")
	if err != nil {
		t.Error("Cant evaluate gval!")
	}
	e := Toggler{
		parsed,
		&onTrue,
		&onFalse,
	}
	e.Run(&dbPublic, &dbPrivate)
	if !called {
		t.Error("Not called onFalse!")
	}
}
