package runners

import (
	"encoding/json"
	"errors"

	"bitbucket.org/rewle/ebf/pkg/buses"
)

//go:generate go run generate.go

type Runner func(public, private *buses.Bus)

func (c *Runner) UnmarshalJSON(data []byte) error {
	s := ""
	err := json.Unmarshal(data, &s)
	if err != nil {
		return err
	}

	if runners[s] == nil {
		return errors.New("runner not found")
	}

	r, err := runners[s](data)

	if err != nil {
		return err
	}

	*c = r

	return nil
}

type createRunner func(item json.RawMessage) (Runner, error)
