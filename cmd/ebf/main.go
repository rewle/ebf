package main

import (
	"encoding/json"
	"io/ioutil"
	"os"

	"bitbucket.org/rewle/ebf/pkg/blocks"
	"bitbucket.org/rewle/ebf/pkg/buses"
	"bitbucket.org/rewle/ebf/pkg/entries"
	"bitbucket.org/rewle/ebf/pkg/utils"
)

// todo Compile this

type Config struct {
	BusCreator buses.BusCreator    `json:"databus"`
	Entry      json.RawMessage     `json:"entry"`
	Blocks     blocks.DecodedBlock `json:"blocks"`
}

func main() {
	cfgName := os.Getenv("CONFIG")
	if cfgName == "" {
		panic("You need to specify CONFIG env variable")
	}

	cfgFile, err := os.Open(cfgName)
	utils.PanicOnErr(err)
	cfg, err := ioutil.ReadAll(cfgFile)
	utils.PanicOnErr(err)

	cfgFile.Close()

	var parsedCfg Config
	err = json.Unmarshal(cfg, &parsedCfg)
	utils.PanicOnErr(err)

	entry, err := entries.GetEntry(parsedCfg.Entry, parsedCfg.BusCreator, parsedCfg.Blocks.Inner)
	utils.PanicOnErr(err)

	entry.Serve()
}
