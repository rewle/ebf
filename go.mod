module bitbucket.org/rewle/ebf

go 1.13

require (
	github.com/PaesslerAG/gval v1.0.1
	github.com/kr/pretty v0.1.0 // indirect
	github.com/pkg/errors v0.8.1 // indirect
	github.com/stretchr/testify v1.4.0 // indirect
	go.uber.org/atomic v1.2.0 // indirect
	go.uber.org/multierr v1.1.0 // indirect
	go.uber.org/zap v1.9.1
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
)
