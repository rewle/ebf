RUN_CFG ?= "$(shell pwd)/scripts/run.json"
BUS_CFG ?= "$(shell pwd)/scripts/bus.json"
ENTRY_CFG ?= "$(shell pwd)/scripts/entry.json"

generate:
	RUN_CFG=${RUN_CFG} BUS_CFG=${BUS_CFG} ENTRY_CFG=${ENTRY_CFG} go generate ./...


build: generate
	go build ./cmd/ebf/main.go

example:
	CONFIG=./scripts/example.json ./main
